<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Cvotes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class CommentController extends BaseController
{
  public function __construct()
  {
    parent::__construct();
  }
  
  public function storeComment(Request $request)
  {
    try {
      $comment = new Comment;
      $comment->event_id = $request->event_id;
      $comment->comment = $request->comment;
      $comment->parent = $request->parent;
      $comment->user_id = Auth::id();
      $comment->save();
      if ($comment) {
        Cache::flush();
        return view('eventCommentTemplate', ['comment' => Comment::find($comment->id)]);
      } else {
        return response()->json(['status', 0]);
      }
    } catch (\Exception $exception) {
      return response()->json($exception->getMessage());
    }
    
  }
  public function deleteComment(Request $request)
  {
    $comment_id = $request->id;
    Comment::where('id', $request->id)->orWhere('parent', $request->id)->delete();
    Cvotes::where('comment_id', $comment_id)->delete();
    Cache::flush();
  }
  public function replyComment(Request $request){
    $comment = new Comment;
    $comment->event_id = $request->event_id;
    $comment->comment = $request->comment;
    $comment->parent = $request->parent;
    if ($request->stream_id) {
      $comment->stream_id = $request->stream_id;
    }
    $comment->user_id = Auth::id();
    $comment->save();
    if ($comment) {
      Cache::flush();
      return view('eventCommentReplyTemplate', ['reply' => Comment::find($comment->id)]);
    } else {
      return response()->json(['status', true]);
    }
  }
  
  public function updateComment(Request $request)
  {
    $comment = Comment::find($request->pk);
    $comment->comment = $request->value;
    $comment->save();
    Cache::flush();
  }
  
  public function voteComment(Request $request){
    $commentId = $request->comment_id;
    if (Cvotes::where(['comment_id' => $commentId, 'user_id' => Auth::id()])->count() > 0 ||  Cvotes::where(['comment_id' => $commentId, 'ip' => $this->get_client_ip()])->count() > 0) {
      return response()->json(['msg' => 'you already voted!']);
    }
    elseif (Comment::where(['id' => $commentId])->first()->user_id == Auth::id()) {
      return response()->json(['msg' => 'You can\'t vote on your own comment!']);
    }else{
      $vote = new Cvotes;
      $vote->user_id = Auth::id();
      $vote->ip = $this->get_client_ip();
      $vote->comment_id = $commentId;
      $vote->save();
      Cache::flush();
    }
    
  }
  
  public function voteCommentDown(Request $request)
  {
    $commentId = $request->comment_id;
    if (Cvotes::where(['comment_id' => $commentId, 'user_id' => Auth::id()])->count() == 0) {
      return response()->json(['msg' => 'you didn\'t vote yet!']);
    } elseif (Comment::where(['id' => $commentId])->first()->user_id == Auth::id()) {
      return response()->json(['msg' => 'You can\'t vote on your own comment!']);
    } else {
      Cvotes::where(['comment_id' => $commentId, 'user_id' => Auth::id()])->delete();
      Cache::flush();
    }
  }
  
  public function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
  }
  
}
