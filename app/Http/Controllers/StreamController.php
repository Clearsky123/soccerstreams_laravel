<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Evaluation;
use App\Event;
use App\Stream;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class StreamController extends BaseController
{
  public function __construct()
  {
    parent::__construct();
  }
  
  public function showMatchStreams()
  {
    $eventId="7040";
    $languages = DB::table('languages')->get();

    $event = Cache::remember('event_' . $eventId, 3, function () use ($eventId) {
      return Event::getEventInfo($eventId);
    });
    if (is_null($event)) {
      return redirect('/')->with('error', 'This match no longer available!');
    } else {

      if( strtotime( $event->end_date) + 2 * 60 * 60 < time()  )
      {
          DB::table('streams')->where('event_id', '=', $eventId)->delete();
          Cache::flush();
      }
      
      $allStreams = Cache::remember('streams_' . $eventId, 3, function () use ($eventId) {
        return Stream::getAllEventStreams($eventId, Auth::check() ? Auth::id() : 0);
      });
      
      $comments = Cache::remember('event_comments_' . $eventId, 3, function () use ($eventId) {
        return Comment::getEventComments($eventId);
      });

      $commentsCount = Comment::where(['event_id' => $eventId ])->count();
      $hComments = $this->_buildTree($comments);
      $streamTypes = $this->_extractStreamsTypes($allStreams);
      $streamQuality = $this->_extractStreamsQuality($allStreams);
      $streamLanguage = $this->_extractStreamsLanguage($allStreams);
      return view('streams', [
        'allStreams' => $allStreams,
        'event' => $event,
        'streamTypes' => $streamTypes,
        'streamQuality' => $streamQuality,
        'streamLanguage' => $streamLanguage,
        'comments' => $comments,
        'comment_count' => $commentsCount,
        'hComments' => $hComments,
        'languages' => $languages
      ]);
    }
    
  }
  
  public static function getComments($streamId)
  {
    return Cache::remember('stream_comments_' . $streamId, 1, function () use ($streamId) {
      $commentsArray =  Comment::getStreamComments($streamId)->toArray();
      return self::_buildTree($commentsArray);
    });
  }
  
  public function showEventStreams(Request $request, $eventId)
  {
    
    $event = Event::getEventInfo($eventId);
    if (is_null($event)) {
      return redirect('/')->with('error', 'This event no longer available!');
    } else {
      $vStreams = Stream::getStreams($eventId, 1);
      $streams = Stream::getStreams($eventId, 0);
      $streamTypes = $this->_extractStreamTypes($vStreams, $streams);
      $streamQuality = $this->_extractStreamQuality($vStreams, $streams);
      $streamLanguage = $this->_extractStreamLanguage($vStreams, $streams);
      return view('eventStreams', [
        'vStream' => $vStreams,
        'streams' => $streams,
        'event' => $event,
        'streamTypes' => $streamTypes,
        'streamQuality' => $streamQuality,
        'streamLanguage' => $streamLanguage,
      ]);
    }
    
  }
  
  public function voteStream(Request $request)
  {
    $streamId = $request->stream;
    $eventId = $request->eventId;
    $evaluation = new Evaluation;
    $evaluation->user_id = Auth::id();
    $evaluation->stream_id = $streamId;
    $evaluation->eval_type = 1;
    $evaluation->save();
    Cache::forget('streams_' . $eventId);
    Cache::flush();
  }
  
  public function recommendStreamByModerator(Request $request)
  {
    $streamId = $request->stream;
    $mdStream = Stream::find($streamId);
    $mdStream->mod_recommended = $request->action;
    $mdStream->save();
    Cache::forget('streams_' . $request->event);
    Cache::flush();
  }
  
  public function reportStream(Request $request)
  {
    $eventId = $request->eventId;
    $streamId = $request->stream;
    $evaluation = new Evaluation;
    $evaluation->user_id = Auth::id();
    $evaluation->stream_id = $streamId;
    $evaluation->comment = $request->comment;
    $evaluation->eval_type = 0;
    $evaluation->save();
    Cache::forget( 'streams_'.$eventId );
    Cache::flush();
  }
  
  private function _extractStreamTypes($vStreams, $streams)
  {
    $streamTypes = [];
    foreach ($streams as $stream) {
      if (!in_array($stream->stream_type, $streamTypes)) {
        array_push($streamTypes, $stream->stream_type);
      }
    }
    foreach ($vStreams as $stream) {
      if (!in_array($stream->stream_type, $streamTypes)) {
        array_push($streamTypes, $stream->stream_type);
      }
    }
    
    return $streamTypes;
  }
  
  private function _extractStreamQuality($vStreams, $streams)
  {
    $streamQuality = [];
    foreach ($streams as $stream) {
      if (!in_array($stream->quality, $streamQuality)) {
        array_push($streamQuality, $stream->quality);
      }
    }
    foreach ($vStreams as $stream) {
      if (!in_array($stream->quality, $streamQuality)) {
        array_push($streamQuality, $stream->quality);
      }
    }
    
    return $streamQuality;
  }
  
  private function _extractStreamLanguage($vStreams, $streams)
  {
    $streamLangs = [];
    foreach ($streams as $stream) {
      if (!in_array($stream->language, $streamLangs)) {
        array_push($streamLangs, $stream->language);
      }
    }
    foreach ($vStreams as $stream) {
      if (!in_array($stream->language, $streamLangs)) {
        array_push($streamLangs, $stream->language);
      }
    }
    
    return $streamLangs;
  }
  
  private function _extractStreamsTypes($streams)
  {
    $streamTypes = [];
    foreach ($streams as $stream) {
      if (!in_array($stream->stream_type, $streamTypes)) {
        array_push($streamTypes, $stream->stream_type);
      }
    }
    
    return $streamTypes;
  }
  
  private function _extractStreamsQuality($streams)
  {
    $streamQuality = [];
    foreach ($streams as $stream) {
      if (!in_array($stream->quality, $streamQuality)) {
        array_push($streamQuality, $stream->quality);
      }
    }
    
    return $streamQuality;
  }
  
  private function _extractStreamsLanguage($streams)
  {
    $streamLangs = [];
    foreach ($streams as $stream) {
      if (!in_array($stream->language, $streamLangs)) {
        array_push($streamLangs, $stream->language);
      }
    }
    
    return $streamLangs;
  }
  
  private static function _buildTree($elements, $parentId = 0)
  {
    $branch = array();
    
    foreach ($elements as $element) {
      if ($element->parent == $parentId) {
        $children = self::_buildTree($elements, $element->id);
        if ($children) {
          $element->replies = $children;
        }
        $branch[] = $element;
      }
    }
    
    return $branch;
  }
  
  public function checkBanDomain(Request $request)
  {
    
    $url = $request->url;
    $event_id = $request->eventId;
    $domains = DB::table('domains')->get();
    foreach ($domains as $domain) {
      if ((strpos($url, $domain->domain) !== false) && $domain->ban == 1) {
        echo 0;
        exit;
      }
    }
    
    $urls = DB::table('streams')->where('url', '=', $url)->where('event_id', '=', $event_id)->get();
    if (count($urls)) {
      echo 1;
      exit;
    }
    echo 2;
  }

  public function getStreamInfo( Request $request )
  {
      $stream_id = $request->stream_id;
      
      echo json_encode( Stream::getStreamById( $stream_id ));
  }

  public function deleteStream($streamId)
  {
    $stream = Stream::find($streamId);
    
    if ($stream->user_id != Auth::id()) {
      echo 0;
      return;
    }
    
    $stream->delete();
    parent::registerLog('Delete Stream ID: ' . $streamId, 3);
    
    Cache::forget('streams_' . $stream->event_id);
    Cache::flush();
    echo 1;
  }
}
