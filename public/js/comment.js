$('[data-toggle="collapse"]').on('click', function () {
  var $this = $(this),
    $parent = $this.data('parent');
  if ($this.data('parent') === undefined) { /* Just toggle my  */
    $this.find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');
    return true;
  }
  
  /* Open element will be close if parent !== undefined */
  var currentIcon = $this.find('.glyphicon');
  currentIcon.toggleClass('glyphicon-plus glyphicon-minus');
  $parent.find('.glyphicon').not(currentIcon).removeClass('glyphicon-minus').addClass('glyphicon-plus');
  
});
$(function ($) {
  
  $.fn.editable.defaults.params = function (params) {
    params._token = $("#_token").data("token");
    return params;
  };
  $.fn.editable.defaults.mode = 'inline';
  $('.editable').editable();
  
  $('body').delegate('#postComment', 'submit', function (e) {
    e.preventDefault();
    if ($(this).context[3].value != '') {
      var form = $(this);
      var formAction = $(this).attr("action");
      var data = $(this).serialize();
      axios.post(formAction, data)
        .then(function (response) {
          $('#comments-div').prepend(response.data);
          if ($('.no-comments').length) {
            $('.no-comments').remove();
          }
          form[0].reset();
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  });
  
  $('body').delegate('.comment-reply-form', 'submit', function (e) {
    e.preventDefault();
    if ($(this).context[3].value != '') {
      var replyForm = $(this);
      var formAction = $(this).attr("action");
      var parent = replyForm.context[2].value;
      var data = $(this).serialize();
      axios.post(formAction, data)
        .then(function (response) {
          if ($("#replies_" + parent).length) {
            $('#replies_' + parent).prepend(response.data);
          }else{
            $("#comment_" + parent).append('<div id="replies_'+ parent +'">'+response.data+'</div>');
          }
          replyForm[0].reset();
          replyForm.parent().parent().find('.reply_button').click();
          $('.parent_comment .replies').first().css('margin-left', '10px');
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  });
  
});

function editComment(e,commentId) {
  e.stopPropagation();
  e.preventDefault();
  $('#commentContent_' + commentId).editable('toggle');
}
function validateEdit(value){
  alert('hi');
  if($.trim(value) == '') {
    return 'This field is required';
  }
}
function deleteComment(id){
  axios.post("/deleteComment", {commentId:id})
        .then(function (response) {
          if (response.data.deleted) {
            $("#comment_"+ id).remove();
          }
        })
        .catch(function (error) {
          console.log(error);
        });
}