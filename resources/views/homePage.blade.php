@extends('master')
@section('headScripts')
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.13/r-2.1.1/datatables.min.css"/>
  <script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.13/r-2.1.1/datatables.min.js"></script>
@endsection
@section('style')
  <style>
    .borderTop{
      border-top: 2px solid #f39c12 !important;
    }
    .matches_divider{ width:0 !important; }
    @media screen and (-webkit-min-device-pixel-ratio:0) {
      /*your rules for chrome*/
      .sorting_disabled{ width:0 !important; }
    }
  </style>
@endsection
@section('content')
  
  @if (session('error'))
    <div class="alert alert-danger" style="margin: 10px;">
      {{ session('error') }}
    </div>
  @endif
  <div class="row">
    <div class="col-md-7 filter">
      <form action="" class="form-inline">
        <div class="form-group">
          <label style="margin-top: 7px" for="competition_selector">Competition </label>
            <select class="form-control" id="competition_selector">
              <option value="">All</option>
              @foreach($competitions as $competition)
                <option value="{{ $competition }}">{{ $competition }}</option>
              @endforeach
            </select>
        </div>
      </form>
    </div>
    <?php
      $updateScore = "checked";
      if(session('updateScore') != null)
        $updateScore = session('updateScore') == "true" ? "checked" : "";
    ?>

    <div class="col-md-5" style="text-align: right;">
      <div class="checkbox">
        <label>
          <input type="checkbox" id="updateScore" {{ $updateScore }}> Show scores
        </label>
      </div>
    </div>
  </div>
  <div class="row" style="color: red;font-size: 40px;background-color: gray;">
      <a href="/streams"> Stream View</a>

  </div>
  <div id="manage_events">
    <div class="table-responsive">
      <table class="table table-striped table-hover" id="eventsTable" width="100%">
      <thead>
        <tr>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <?php $lastDate = '' ?>
        @foreach($events as $event_parent)
          @foreach($event_parent as $event)
            @if($lastDate != \Carbon\Carbon::parse($event->start_date)->addMinute(Session::get('visitorTZ')*60)->format('d M Y'))
              <tr>
                <td class="matches_divider" colspan="4">
                  <i class="fa fa-calendar fa-2x"></i>
                  <span>{{ \Carbon\Carbon::parse($event->start_date)->addMinute(Session::get('visitorTZ')*60)->format('d M Y') }}</span>
                </td>
                <td class="matches_divider"></td>
                <td class="matches_divider"></td>
                <td class="matches_divider"></td>
              </tr>
            @endif
            <?php
              $cur_time = \Carbon\Carbon::now();
              $start_time = \Carbon\Carbon::parse($event->start_date);
              $isRunning = "not-running";
              if($cur_time >= $start_time)
                $isRunning = "running";

              $lastDate = \Carbon\Carbon::parse($event->start_date)->addMinute(Session::get('visitorTZ')*60)->format('d M Y'); 

              $borderTop = "";
              if(isset($event->isFav) && $event->isFav == false) $borderTop = "borderTop";
            ?>
            <tr @if(isset($event->isFav) && $event->isFav == true) style=" background-color: rgba(241, 196, 15, 0.3) " @endif>
              <td width="10%" class="{{ $borderTop }}">
                {{-- @if(empty(trim($event->game_minute))) --}}
                @if(\Carbon\Carbon::parse($event->start_date) > \Carbon\Carbon::now() ||
                    $updateScore == false ||
                    $event->event_minute == "")
                  <span class="event-time" data-eventtime="{{ $event->start_date }}">
                    {{ \Carbon\Carbon::parse($event->start_date)->addMinute(Session::get('visitorTZ')*60)->format('j M H:i') }}
                  </span>
                @else
                  <span class="@if($event->event_minute != "FT") game_minute @endif {{ $isRunning }}" id="{{ $isRunning }}_{{ $event->event_id }}_minute" data-id="{{ $event->event_id }}">'{{ $event->event_minute }}</span>
                @endif
              </td>
              <td width="10%" class="{{ $borderTop }}">
                <p class="hidden">{{ $event->competition_name }}</p>
                @if( $event->competition_logo && file_exists( 'images/competitions/small/'.$event->competition_logo ) )
                  <img src="{{ cdn('images/competitions/small/'.$event->competition_logo)}}" alt="{{ $event->competition_name }}" width="30 " height="30" title="{{ $event->competition_name }}">
                @else
                  <img src="{{ cdn('images/generic.png') }}" alt="{{ $event->competition_name }}" width="30 " height="30" title="{{ $event->competition_name }}">
                @endif
              </td>
              @if(is_null($event->event_title) || $event->event_title == 'NULL' || empty($event->event_title))
                <td class="text-center {{ $borderTop }}">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="col-md-5 text-right">
                        @if( file_exists( 'images/teams/small/'.$event->home_team_logo ) )
                          {{ $event->home_team }} &nbsp;<img src="{{ cdn('images/teams/small/'.$event->home_team_logo)}}" alt="{{ $event->home_team }}" width="30" height="30">
                        @else
                          {{ $event->home_team }} &nbsp;<img src="{{ cdn('images/generic.png')}}" alt="{{ $event->home_team }}" width="30" height="30">
                        @endif
                      </div>
                      <div class="col-md-2 text-center">
                        {{-- @if(empty(trim($event->event_status))) --}}
                        @if(\Carbon\Carbon::parse($event->start_date) > \Carbon\Carbon::now() || $updateScore == false || $event->event_minute == "" || $event->event_status == null)
                          vs
                        @else
                          <span id="{{ $isRunning }}_{{ $event->event_id }}_score">{{ $event->event_status }}</span>
                        @endif
                      </div>
                      <div class="col-md-5 text-left">
                        @if( file_exists( 'images/teams/small/'.$event->away_team_logo ) )
                          <img src="{{ cdn('images/teams/small/'.$event->away_team_logo )}}" alt="{{ $event->away_team }}" width="30" height="30">&nbsp;{{ $event->away_team }}
                        @else
                          <img src="{{ cdn('images/generic.png')}}" alt="{{ $event->away_team }}" width="30" height="30">&nbsp;{{ $event->away_team }}
                        @endif
                      </div>
                    </div>
                  </div>
                </td>
              @else
                <td class="text-center">{{ $event->event_title }}</td>
              @endif
              <td width="15%" class="text-center {{ $borderTop }}">
                @if(is_null($event->event_title) || $event->event_title == 'NULL' || empty($event->event_title))
                  <a href="{{ secure_url('streams/'.$event->event_id.'/'.$event->home_team_slug.'_vs_'.$event->away_team_slug) }}" class="btn btn-rss">
                  Watch
                  @if($event->streams != 0)
                    <span class="count">{{ $event->streams }}</span>
                  @endif
                  </a>
                @else
                  {{--<a href="{{ secure_url('eventStreams/'.$event->event_id) }}" class="btn btn-rss">--}}
                  <a href="{{ secure_url('streams/'.$event->event_id.'/'.$event->event_title) }}" class="btn btn-rss">
                  Watch
                  @if($event->streams != 0)
                    <span class="count">{{ $event->streams }}</span>
                  @endif
                  </a>
                @endif
                <!-- Add star icon next to favourite team/com. event -->
                @if(isset($event->isFav) && $event->isFav == true)
                  <i class="fa fa-star fa-1x" aria-hidden="true" style="color: #00222E; font-size: 130%; cursor: default; padding-left: 20px;" title="Favourites"></i>
                @else
                  <i class="fa fa-fw"></i>
                  <i class="fa fa-fw"></i>
                @endif
              </td>
            </tr>
          @endforeach
          
        @endforeach
        </tbody>
        <tfoot></tfoot>
      </table>
    </div>
  </div>
@endsection
@section('scripts')
  <script>
    $(function ($) {
      if (typeof currentZoneOffset !== 'undefined') {
        momentZone = moment.tz.guess();
        currentZoneOffset = moment.tz(momentZone).utcOffset() / 60;
        tz = currentZoneOffset;
      } else {
        var tzOptions = document.getElementById('offset');
        var tz = tzOptions.options[tzOptions.selectedIndex].value;
      }
      var table = $('#eventsTable').DataTable({
        responsive: true,
        paging: false,
        info: false,
        ordering: false,
        orderCellsTop: true,
        dom: 'lrtip'
      });
      
      $('#eventsTable').on('click', 'tbody tr', function () {
        if ($(this).find('td').hasClass('matches_divider')) {
          return;
        }
        window.location.href = $(this).find('.btn-rss').attr('href');
      });
      
      $('#competition_selector').on('change', function (e) {
        table.search(this.value).draw();
      });

      table.search( $('#competition_selector').val() ).draw();
    });

    function notificationAction(el) {
      $.post(
        '{{ secure_url('profile/notificationAction') }}',
        {"_token": "{{ csrf_token() }}", "id": el, "action": 1},
        function (data, status) {
          console.log(data)
        });
    }

    function verifyAlert()
    {
      $.post(
        '{{ secure_url('profile/closeAlert') }}',
        {"_token": "{{ csrf_token() }}"},
        function (data, status) {
          console.log(data)
        });
    }

    function updateScore(updateCheck){
      if(document.getElementById('updateScore').checked){
        var data = [];
        $('.running').each(function(i, obj){
          data.push($(obj).data('id'));
        })

        $.ajax({
          url: document.location.origin + '/updatehomepagescores',
          dataType: 'json',
          data: {
            data: data
          },
          success: function (result) {
            if( !(result['result'] == 0) )
              for (var i = result['result'].length - 1; i >= 0; i--) {
                if(result['result'][i]["event_minute"] != null)
                  $("#running_"+result['result'][i]["event_id"]+"_minute").text("'"+result['result'][i]["event_minute"]);
                if(result['result'][i]["event_status"] != null)
                  $("#running_"+result['result'][i]["event_id"]+"_score").text(result['result'][i]["event_status"]);
              }
          },
          error: function(result) {
          }
        });
      }
    }

    updateScore();

    setInterval(function(){
      updateScore();
    }, 60000)

    $("#updateScore").change(function(){
      var check = document.getElementById('updateScore').checked;
      var url = document.location.origin + '/updatehomepagescores';
      window.location = url + "?check=" + check;
    })
  </script>
@endsection