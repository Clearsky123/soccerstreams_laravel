<div class="media"  id="comment_{{ $reply->id }}">
  <!-- answer to the first comment -->
  <div class="media-heading">
    <button class="btn btn-default btn-collapse btn-xs collapse_button" data-parent="#comment_{{ $reply->id }}" type="button" data-toggle="collapse" data-target="#commentId_{{ $reply->id }}"
            aria-expanded="false"
            aria-controls="collapseExample"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></button>
    <span class="done" style="display: {{ ($reply->is_voted) ? 'inline':'none' }}" onclick="CommentVoteDown(this,'{{ $reply->id }}')">
        <i class="fa fa-check-circle-o fa-2x" aria-hidden="true"></i>
    </span>
    <span class="vot" style="display: {{ ($reply->is_voted) ? 'none':'inline' }}" onclick="CommentVoteUp(this,'{{ $reply->id }}')">
        <i class="fa fa-arrow-circle-up fa-2x" style="color: green" aria-hidden="true"></i>
    </span>
    <span class="label label-gold-rss user_name">{{ \Illuminate\Support\Facades\Auth::user()->name }}</span>
    @if(\Illuminate\Support\Facades\Auth::check())
      @if($reply->role==1)
        (Moderator)
      @elseif($reply->role==2)
        (Admin)
      @endif
    @endif
    <b>
      <span class="votes_count">{{ ($reply->votes)?$reply->votes:'0' }}</span>
      votes
    </b>
    &nbsp;{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $reply->created_at)->diffForHumans() }}
  </div>
  <div class="panel-collapse collapse in" id="commentId_{{ $reply->id }}">
    <!-- media-left -->
    <div class="media-body">
      <div id="commentContent_{{ $reply->id }}" data-validate="validateEdit(value)" class="comment_content editable" data-pk="{{ $reply->id }}" data-type="textarea" data-url="{{ secure_url('updateComment') }}"
           data-toggle="manual" data-title="Enter comment" data-placement="top" data-inputclass="form-control">
              {{ $reply->comment }}
      </div>
      <div class="comment-meta">
        @if(\Illuminate\Support\Facades\Auth::id() == $reply->user_id)
          <span>
            <a href="javascript:void(0)" onclick="editComment(event,'{{ $reply->id }}')">Edit</a>
          </span>
        @endif
        @if(\Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->role>=1)
          <span><a class="delete_button" href="javascript:void(0)" onclick="deleteComment({{ $reply->id }})">Delete</a></span>
        @endif
        @if(\Illuminate\Support\Facades\Auth::check())
            <span><a class="reply_button" role="button" data-toggle="collapse" href="#replyComment_{{ $reply->id }}" aria-expanded="false" aria-controls="collapseExample">reply</a></span>
            <div class="collapse" id="replyComment_{{ $reply->id }}">
              <form class="comment-reply-form" method="post" action="{{ secure_url('replyComment') }}">
                {{ csrf_field() }}
                <input type="hidden" name="event_id" value="{{ $reply->event_id }}">
                <input type="hidden" name="parent" value="{{ $reply->id }}">
                @if(isset($stream))
                  <input type="hidden" name="stream_id" value="{{ $stream->stream_id }}">
                @endif
                <div class="form-group">
                  <label for="comment">Your Reply</label>
                  <textarea name="comment" class="form-control" rows="3" required></textarea>
                </div>
                <button type="submit" class="btn btn-default">Send</button>
              </form>
            </div>
          @else
            <span><a class="reply_button" href="" onclick="event.preventDefault();sweetAlert('Oops...', 'Only registered user have the ability to reply on comments!', 'error');">reply</a></span>
          @endif
          
      </div>
      <!-- comment-meta -->
    </div>
  </div>
  <!-- comments -->
</div>