<div class="media{{ (!$comment->parent)?' parent_comment':'' }}{{ (isset($stream) && !isset($reply))?' stream_comment':'' }}" data-votes="{{ $comment->votes }}" id="comment_{{ $comment->id }}">
  <div class="media-heading">
    <button class="btn btn-default btn-xs collapse_button" type="button" data-parent="#comment_{{ $comment->id }}" data-toggle="collapse" data-target="#commentId_{{ $comment->id }}" aria-expanded="false" aria-controls="collapseExample">
      <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
    </button>
    <span class="done" style="display: {{ ($comment->is_voted) ? 'inline':'none' }}" onclick="CommentVoteDown(this,'{{ $comment->id }}')">
        <i class="fa fa-check-circle-o fa-2x" aria-hidden="true"></i>
    </span>
    <span class="vot" style="display: {{ ($comment->is_voted) ? 'none':'inline' }}" onclick="CommentVoteUp(this,'{{ $comment->id }}')">
        <i class="fa fa-arrow-circle-up fa-2x" style="color: green" aria-hidden="true"></i>
    </span>
    <span class="label label-gold-rss user_name">{{ $comment->name }}</span>
    @if(\Illuminate\Support\Facades\Auth::check())
      @if($comment->role==1)
        <small>(Moderator)</small>
      @elseif($comment->role==2)
        <small>(Admin)</small>
      @endif
    @endif
    <b>
      <span class="votes_count" data-count="{{ ($comment->votes) ? $comment->votes : '0' }}">
        {{ ($comment->votes) ? $comment->votes : '0' }}
      </span>
      votes
    </b>
    &nbsp;{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $comment->created_at)->diffForHumans() }}
  </div>
  <div class="panel-collapse collapse in" id="commentId_{{ $comment->id }}">
    <div class="media-body">
      @if(isset($stream) && !isset($reply))
        @if(empty($stream->language_flag))
          <?php $flag = 'unknown'; ?>
        @else
          <?php $flag = $stream->language_flag; ?>
        @endif
        <div class="stream_comment_row stream_{{ $stream->stream_id }} @if(strtolower($stream->compatibility)=='no') hidden-xs @endif" data-href="{{ $stream->url }}"
          data-stream-id="{{ $stream->stream_id }}" data-type="{{ strtoupper($stream->stream_type) }}" data-quality="{{ strtoupper($stream->quality) }}"
          data-language="{{ strtoupper($stream->language_name) }}" data-mobile="{{ $stream->compatibility }}">
        <td width="7%" class="rating">
          <span class="rate">
            {{ $stream->vote }}
          </span>
          @if(\Illuminate\Support\Facades\Auth::guest())
            <a href="javascript:void(0);" onclick="sweetAlert('Oops...', 'Only registered user have the ability to vote streams!', 'error');">
              <i class="fa fa-thumbs-up fa-2x" style="color: green;margin: 0;vertical-align: top;" aria-hidden="true"></i>
            </a>
          @else
            @if(is_null($stream->is_voted))
              <span class="vot">
                <a href="javascript:void(0);" onclick="voteUp(this,'{{ $stream->stream_id }}')">
                  <i class="fa fa-thumbs-up fa-2x" style="color: green;margin: 0;vertical-align: top;" aria-hidden="true"></i>
                </a>
              </span>
            @else
              <span class="done"><i class="fa fa-check-circle-o fa-2x" aria-hidden="true"></i></span>
            @endif
            <span class="done" style="display: none">
              <i class="fa fa-check-circle-o fa-2x" aria-hidden="true"></i>
            </span>
          @endif
        </td>
        <td width="5%" class="">
          @if(strtolower($stream->stream_type)=='acestream' || strtolower($stream->stream_type)=='vlc' || strtolower($stream->stream_type)=='sopcast')
            <button data-clipboard-text="{{ $stream->url }}" class="btn btn-rss btn-copy">
              <i class="fa fa-clipboard" aria-hidden="true"></i>
            </button>
          @else
            <a href="{{ $stream->url }}" target="_blank" class="btn btn-rss">
              <i class="fa fa-play-circle-o" aria-hidden="true"></i>
            </a>
          @endif
        </td>
        <td width="1%">
          <a class="btn-copy" data-toggle="tooltip" data-placement="bottom" data-original-title="{{ $stream->other_info }}">
            @if( $stream->other_info )<i class="fa fa-info-circle"></i>
            @endif
          </a>
        </td>

        <td class="clickable">
          <img src="{{ secure_asset('images/languages/'.$flag.'.png') }}" alt="{{ $stream->language_flag }}">
          <p class="hidden languageValue">{{ $stream->language_name }}</p>
        </td>
        <td class="clickable" width="40%">
          {{ $stream->username }}
          @if( $stream->approved == 1 )
              <span verified-hover-position="top" class="tag verified approved"><b>APPROVED STREAMER</b></span>
          @endif
          @if($stream->verified_user==1)
            <span verified-hover-text="Verified Streamers are handpicked and represent the highest quality and/or most stable streams on Soccer Streams"
                  verified-hover-position="top"
                  class="tag verified"><b>VERIFIED STREAMER</b></span>
          @endif
        </td>
        <td class="clickable">
          @if(strtolower($stream->stream_type)=='vlc')
            <span class="tag stream-type-tag">VLC</span>
          @elseif(strtolower($stream->stream_type)=='acestream')
            <span class="tag stream-type-tag">ACE</span>
          @elseif(strtolower($stream->stream_type)=='sopcast')
            <span class="tag stream-type-tag">SOP</span>
          @elseif(strtolower($stream->stream_type)=='http')
            <span class="tag stream-type-tag">HTTP</span>
          @else
            <span class="tag stream-type-tag">Other</span>
          @endif
          <p class="hidden">{{ $stream->stream_type }}</p>
        </td>
        <td class="clickable">
          @if(strtolower($stream->quality)=='hd' || strtolower($stream->quality)=='sd')
            <span class="tag stream-type-tag qualityValue">{{ $stream->quality }}</span>
          @elseif(strtolower($stream->quality)=='520p')
            <span class="tag quality-tag qualityValue">520</span>
          @else
            <span class="tag unknown quality-tag"></span>
          @endif
        </td>
        <td class="clickable hidden-xs">
          @if(strtolower($stream->compatibility)=='no')
            <img class="small_icon" src="{{ secure_asset('icons/streaminfo/mobilecompatno.png') }}" alt="In compatible" title="Not a mobile compatible">
          @else
            <img class="small_icon" src="{{ secure_asset('icons/streaminfo/mobilecompat.png') }}" alt="compatible" title="Mobile Compatible">
          @endif
        </td>
        <td>
          <span class="tag ad_number">{{ $stream->ad_number>0?$stream->ad_number.' Ad-overlays':'no Ad-overlays' }}</span>
        </td>
        <td>
          @if($stream->nsfw==1)
            <span class="tag nsfw-tag">NSFW</span>
          @endif
        </td>
        <td>
          <a data-slug="{{ $stream->username }}_{{ $stream->stream_id }}" data-clipboard-text="{{ Request::url() }}#{{ $stream->username }}_{{ $stream->stream_id }}" class="btn-copy permalink" data-toggle="tooltip" data-placement="bottom" data-original-title="Copy stream permalink">
              <i class="fa fa-share-square-o" aria-hidden="true"></i>
          </a>
        </td>
        <td>
          @if(Auth::guest())
            <a href="javascript:void(0);" onclick="sweetAlert('Oops...', 'Only registered user have the ability to vote streams!', 'error');">
              <i class="fa fa-exclamation-triangle" style="color: red" aria-hidden="true"></i>
            </a>
          @else
            @if(is_null($stream->is_reported))
              <a href="javascript:void(0);" onclick="report(this,'{{ $stream->stream_id }}','{{ $stream->event_id }}')" title="Report stream">
                <i class="fa fa-exclamation-triangle" style="color: red" aria-hidden="true"></i>
              </a>
            @else
              <span><i class="fa fa-check" aria-hidden="true"></i></span>
            @endif
            <span style="display: none"><i class="fa fa-check" aria-hidden="true"></i></span>
          @endif
          
        </td>
      </div><br>
      @endif
      <div id="_token" class="hidden" data-token="{{ csrf_token() }}"></div>
      <div id="commentContent_{{ $comment->id }}" class="comment_content editable" data-pk="{{ $comment->id }}" data-type="textarea" data-url="{{ secure_url('updateComment') }}"
           data-toggle="manual" data-title="Enter comment" data-placement="top" data-inputclass="form-control">
          {!! nl2br($comment->comment) !!}
      </div>
      <div class="comment-meta">
        
        @if(\Illuminate\Support\Facades\Auth::id() == $comment->user_id)
          <span>
            <a href="javascript:void(0)" onclick="editComment(event,'{{ $comment->id }}')">Edit</a>
          </span>
        @endif
        @if(\Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->role>=1)
          <span><a class="delete_button" href="javascript:void(0)" onclick="deleteComment({{ $comment->id }})">Delete</a></span>
        @endif
        @if(\Illuminate\Support\Facades\Auth::check())
          <span><a class="reply_button" role="button" data-toggle="collapse" href="#replyComment_{{ $comment->id }}" aria-expanded="false" aria-controls="collapseExample">reply</a></span>
          <div class="collapse" id="replyComment_{{ $comment->id }}">
            <form class="comment-reply-form" method="post" action="{{ secure_url('replyComment') }}">
              {{ csrf_field() }}
              <input type="hidden" name="event_id" value="{{ $event_id }}">
              <input type="hidden" name="parent" value="{{ $comment->id }}">
              @if(isset($stream))
                <input type="hidden" name="stream_id" value="{{ $stream->stream_id }}">
              @endif
              <div class="form-group">
                <label for="comment">Your Reply</label>
                <textarea name="comment" class="form-control" rows="3" required></textarea>
              </div>
              <button type="submit" class="btn btn-default">Send</button>
            </form>
          </div>
        @else
            <span><a class="reply_button" onclick="event.preventDefault();sweetAlert('Oops...', 'Only registered user have the ability to reply on comments!', 'error');" href="">reply</a></span>
        @endif
        
      </div>
      @if (isset($comment->replies) && count($comment->replies) > 0)
        <div class="replies" id="replies_{{ $comment->id }}">
          @foreach($comment->replies as $reply)
            @include('partials.comment', ['comment'=>$reply, 'reply' => true])
          @endforeach
        </div>
      @endif
    </div>
  </div>
</div>