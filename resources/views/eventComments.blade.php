<div class="panel-group panel-comments" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseComments" aria-expanded="true" aria-controls="collapseOne" role="tab" id="panel-comment" style="cursor:pointer">
        <span class="color-gold">Comments
        <small>({{ $comment_count }})</small></span>
        <i class="fa fa-compress pull-right" aria-hidden="true" style="font-size: 1.5em;"></i>
    </div>
    <div id="collapseComments" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="panel-comment">
      <div class="panel-body">
        <div class="event-comments">
          @if(\Illuminate\Support\Facades\Auth::check())
            {{-- registered user comment box --}}
            <form id="postComment" action="{{ secure_url('storeComment') }}" method="post">
              <input type="hidden" name="event_id" value="{{ $event_id }}">
              <input type="hidden" name="parent" value="0">
              {{ csrf_field() }}
              <div class="form-group">
                <label for="comment">Your Comment</label>
                <textarea name="comment" class="form-control" rows="3"></textarea>
              </div>
              <button type="submit" class="btn btn-default">Send</button>
            </form>
          @else
            <p>Please <a href="{{ secure_url('register') }}">register</a> to add your comment or <a href="{{ secure_url('redditLogin') }}">login with Reddit.</a></p>
          @endif
          {{-- end of registered user comment box --}}
          
          {{-- nested comments --}}
          <div class="row" id="comments-div">
            @if($comments->count()>0)
                @if(count($hComments)>0)
                  @foreach($hComments as $comment)
                    @include('partials.comment', ['comment'=>$comment])
                  @endforeach
                @endif
            @endif
            @if(count($streams))
                @if($allStreams->count())
                  @include('eventStreamsTemplate',['streams'=>$streams, 'comment_show' => true])
                @endif
            @endif
          </div>
          {{-- end of nested comments --}}
        </div>
      </div>
    </div>
  </div>
</div>
<style>
  .editable-pre-wrapped {
    white-space: initial !important;
  }
  
  .event-comments {
    padding-bottom: 9px;
    margin: 5px 0 5px;
  }
  
  .event-comments .comment-meta {
    /*border-bottom: 1px solid #eee;*/
    margin-bottom: 5px;
  }
  
  .event-comments .media {
    border-left: 5px solid #628d9c;
    /*border-bottom: 1px solid #000;*/
    margin-bottom: 5px;
    padding-left: 10px;
  }
  
  .event-comments .media-heading {
    font-size: 12px;
    color: grey;
  }
  
  .event-comments .comment-meta a {
    font-size: 12px;
    color: grey;
    font-weight: bolder;
    margin-right: 5px;
  }
  
  #comments-div {
    padding: 15px;
  }
  
  .label-gold-rss {
    background-color: #B3994C;
  }
  
  a.btn-delete {
    color: #FFF !important;
    padding: 1px;
    background-color: red;
    text-decoration: none;
  }
</style>
<br>
